Kibana docker role
=========

Installs kibana docker-compose project

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install kibana
  hosts: kibana_vms
  become: true
  gather_facts: true
  roles:
    - kibana_docker
      tags:
        - role_kibana_docker
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
